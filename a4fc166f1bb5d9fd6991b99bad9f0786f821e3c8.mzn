include "globals.mzn"; 
int: n = 6;
int: m = 3;
array[1..n] of var 0..10: variables;
array[1..m] of var set of 1..7: partitions;
var 0..9: nvar;
output [
    "nvar = ", show(nvar), ";\n",
    "partitions = ", show(partitions), ";\n",
    "variables = ", show(variables), ";\n"
];
solve satisfy;
predicate partition_set2(array[int] of var set of int: S,
                        var set of int: universe) =
    all_disjoint(S) /\ universe == array_union(i in index_set(S)) ( S[i] )
;
predicate cardinality_atmost_exclude_value(var int: nvar, array[int] of var int: variables, array[int] of var int: values, var set of int: exclude) =
   forall(i in index_set(values)) (
     sum(j in index_set(variables)) (bool2int(not(values[i] in exclude) /\  values[i] = variables[j])) <= nvar
   )
;
predicate cardinality_atmost_partition(var int: nvar, array[int] of var int: variables, array[int] of var set of int: partitions) =
   let {
       int: lbv = min(index_set(variables)),
       int: ubv = max(index_set(variables)),
       int: lbp = min(index_set(partitions)),
       int: ubp = max(index_set(partitions)),
       array[lbv..ubv] of var lbp..ubp+1: selected_partition
   }   
   in
   partition_set2(partitions, array_union(i in lbp..ubp) (partitions[i]))
   /\ % assign a partition index to each value
   forall(i in lbv..ubv) (
      forall(j in lbp..ubp) (
        selected_partition[i] = j  <-> variables[i] in partitions[j]
      )
   )
   /\ % assure that we have atmost nvar occurrences of each partition index 
   cardinality_atmost_exclude_value(nvar, selected_partition, selected_partition, {m+1})
;
predicate cp1d(array[int] of int: x, array[int] of var int: y) =
  assert(index_set(x) = index_set(y),
           "cp1d: x and y have different sizes",
    forall(i in index_set(x)) (
        x[i] = y[i]
    )
  )
; 
predicate cp1d(array[int] of set of int: x, array[int] of var set of int: y) =
  assert(index_set(x) = index_set(y),
           "cp1d: x and y have different sizes",
    forall(i in index_set(x)) (
        x[i] = y[i]
    )
  )
; 
constraint
  cp1d([2, 3, 7, 1, 6, 0], variables)
  /\
  cp1d([
      {1,3},
      {4},
      {2,6}], partitions)
  /\
  nvar = 2
  /\
  cardinality_atmost_partition(nvar, variables, partitions)
;
