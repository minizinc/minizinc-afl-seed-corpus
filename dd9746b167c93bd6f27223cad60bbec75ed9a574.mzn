int: n_machines;                        % The number of machines.
int: n_products;                        % The number of products.
int: n_attributes;                      % The number of attributes.
int: n_steps;                           % The number of plan steps.
set of int: machines = 1..n_machines;
set of int: products = 1..n_products;
set of int: attributes = 1..n_attributes;
set of int: steps = 1..n_steps;
set of machines:                                        input_machines;
set of machines:                                        output_machines;
array [machines, machines]   of 0..1:                   connected;
array [machines, attributes] of 0..1:                   can_add_attr;
array [products, attributes] of 0..1:                   prod_attr_goal;
array [products]             of input_machines:         prod_start_mach;
array [steps, products, attributes] of var 0..1:        step_prod_attr;
array [steps, products]             of var machines:    step_prod_mach;
var steps:                                              last_step;
set of machines: work_machines =
    machines diff (input_machines union output_machines);
constraint
    forall (p in products) (
        step_prod_mach[1, p] = prod_start_mach[p]
    );
constraint
    forall (p in products, a in attributes) (
        step_prod_attr[1, p, a] = 0
    );
constraint
    forall (s in steps, p in products, a in attributes where s < n_steps) (
        step_prod_attr[s, p, a] <= step_prod_attr[s + 1, p, a]
    );
constraint
    forall (s in steps, p, q in products, m in work_machines where p < q) (
        step_prod_mach[s, p] = m  ->  step_prod_mach[s, q] != m
    );
constraint
    forall (p in products, a in attributes) (
        step_prod_attr[last_step, p, a] = prod_attr_goal[p, a]
    );
constraint
    forall (p in products) (
        step_prod_mach[last_step, p] in output_machines
    );
constraint
    forall (s in steps, p in products where s < n_steps) (
        connected[step_prod_mach[s, p], step_prod_mach[s + 1, p]] = 1
    );
constraint
    forall (s in steps, p in products, a in attributes where s < n_steps) (
        step_prod_attr[s + 1, p, a] <=
                step_prod_attr[s, p, a]
              + can_add_attr[step_prod_mach[s, p], a]
    );
last_step = n_steps;
n_machines = 5;
n_products = 2;
n_attributes = 3;
n_steps = 5;
input_machines = {1};
output_machines = {5};
connected =
[|  1, 1, 1, 1, 0,
 |  1, 1, 1, 0, 1,
 |  1, 1, 1, 1, 1,
 |  1, 0, 1, 1, 1,
 |  0, 1, 1, 1, 1
|];
can_add_attr =
[|  0, 0, 0
 |  1, 0, 0
 |  0, 1, 0
 |  0, 0, 1
 |  0, 0, 0
|];
prod_start_mach = [
    1,
    1
];
prod_attr_goal =
[|  1, 1, 0
 |  1, 0, 1
|];
solve satisfy;
output [
    "factory planning instance\n",
    "step | product | location | a1 a2 a3\n",
    "-----+---------+----------+---------\n",
    " 1   | 1       | ", show(step_prod_mach[1, 1]), "        |  ",
        show(step_prod_attr[1, 1, 1]), "  ",
        show(step_prod_attr[1, 1, 2]), "  ",
        show(step_prod_attr[1, 1, 3]), "\n",
    "     | 2       | ", show(step_prod_mach[1, 2]), "        |  ",
        show(step_prod_attr[1, 2, 1]), "  ",
        show(step_prod_attr[1, 2, 2]), "  ",
        show(step_prod_attr[1, 2, 3]), "\n",
    "-----+---------+----------+---------\n",
    " 2   | 1       | ", show(step_prod_mach[2, 1]), "        |  ",
        show(step_prod_attr[2, 1, 1]), "  ",
        show(step_prod_attr[2, 1, 2]), "  ",
        show(step_prod_attr[2, 1, 3]), "\n",
    "     | 2       | ", show(step_prod_mach[2, 2]), "        |  ",
        show(step_prod_attr[2, 2, 1]), "  ",
        show(step_prod_attr[2, 2, 2]), "  ",
        show(step_prod_attr[2, 2, 3]), "\n",
    "-----+---------+----------+---------\n",
    " 3   | 1       | ", show(step_prod_mach[3, 1]), "        |  ",
        show(step_prod_attr[3, 1, 1]), "  ",
        show(step_prod_attr[3, 1, 2]), "  ",
        show(step_prod_attr[3, 1, 3]), "\n",
    "     | 2       | ", show(step_prod_mach[3, 2]), "        |  ",
        show(step_prod_attr[3, 2, 1]), "  ",
        show(step_prod_attr[3, 2, 2]), "  ",
        show(step_prod_attr[3, 2, 3]), "\n",
    "-----+---------+----------+---------\n",
    " 4   | 1       | ", show(step_prod_mach[4, 1]), "        |  ",
        show(step_prod_attr[4, 1, 1]), "  ",
        show(step_prod_attr[4, 1, 2]), "  ",
        show(step_prod_attr[4, 1, 3]), "\n",
    "     | 2       | ", show(step_prod_mach[4, 2]), "        |  ",
        show(step_prod_attr[4, 2, 1]), "  ",
        show(step_prod_attr[4, 2, 2]), "  ",
        show(step_prod_attr[4, 2, 3]), "\n",
    "-----+---------+----------+---------\n",
    " 5   | 1       | ", show(step_prod_mach[5, 1]), "        |  ",
        show(step_prod_attr[5, 1, 1]), "  ",
        show(step_prod_attr[5, 1, 2]), "  ",
        show(step_prod_attr[5, 1, 3]), "\n",
    "     | 2       | ", show(step_prod_mach[5, 2]), "        |  ",
        show(step_prod_attr[5, 2, 1]), "  ",
        show(step_prod_attr[5, 2, 2]), "  ",
        show(step_prod_attr[5, 2, 3]), "\n",
    "-----+---------+----------+---------\n"
];
